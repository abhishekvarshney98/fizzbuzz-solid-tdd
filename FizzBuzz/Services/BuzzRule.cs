﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.Services
{
    public class BuzzRule: IRule
    {
        public bool IsMatch(int input)
        {
            return (input % 5) == 0;
        }

        public string Execute()
        {
            return "Buzz";
        }
    }
}
