﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.Services
{
    public interface IRule
    {
        bool IsMatch(int input);
        string Execute();
        //bool Any();
    }
}
