﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.Services
{
    public interface IFizzBuzzService
    {
        IList<string> GetFizzBuzzNumbers(int input);
    }
}
