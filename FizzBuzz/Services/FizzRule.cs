﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.Services
{
    public class FizzRule:IRule
    {
        public bool IsMatch(int input) 
        {
            return (input % 3) == 0;
        }

        public string Execute()
        {
            return "Fizz";
        }
    }
}
