﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.Services
{
    public class FizzBuzzService: IFizzBuzzService
    {
        private readonly IList<IRule> rules;
        //public FizzBuzzService()
        //{
        //    rules = new List<IRule> { new FizzRule(), new BuzzRule(), new FizzBuzzRule() };
        //}

        public FizzBuzzService(IList<IRule> rules)
        {
            this.rules = rules;
        }

        public IList<string> GetFizzBuzzNumbers(int input)
        {
            var fizzBuzzNumbers = new List<string>();
            for(var i= 1; i<=input ; i++)
            {
                var applicableRules = rules.FirstOrDefault(x => x.IsMatch(input));
                //problem here:
                //var fizzBuzzNumber = applicableRules.Any() ? applicableRules.Execute() : input.ToString();
                var fizzBuzzNumber = applicableRules.IsMatch(input) ? applicableRules.Execute() : input.ToString();
                fizzBuzzNumbers.Add(fizzBuzzNumber);
                //return fizzBuzzNumbers;
            }
            return fizzBuzzNumbers;
        }
    }
}
