﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.Models
{
    public class FizzBuzzViewModel
    {
        [Range(1,1000,ErrorMessage= "Value should be between 1 to 1000")]
        public int input { get; set; }
        public List<string> FizzBuzzNumbers { get; set; }
    }
}
